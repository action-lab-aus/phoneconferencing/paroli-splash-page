import { Button } from "@chakra-ui/react";

interface Props {
  children: string;
  target: string;
  isHero?: boolean;
}

const NavButton = ({ isHero, children, target }: Props) => {
  return (
    <Button
      colorScheme={isHero ? "purple" : "gray"}
      variant={isHero ? "solid" : "ghost"}
      as="a"
      href={target}
    >
      {children}
    </Button>
  );
};

export default NavButton;
