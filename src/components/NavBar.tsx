import { HStack, Image, Show } from "@chakra-ui/react";
import logo from "../assets/logo_smol.png";
import NavButton from "./NavButton";
import "../App.css";

const NavBar = () => {
  return (
    <HStack justify="space-between" className="navBar">
      <Image src={logo} className="logo" />
      <Show above="md">
        <HStack justify="space-between">
          <NavButton target="#about">About</NavButton>
          <NavButton target="#features">Features</NavButton>
          <NavButton target="#usecases">Use Cases</NavButton>
          <NavButton target="#contact" isHero={true}>
            Contact Us
          </NavButton>
        </HStack>
      </Show>
    </HStack>
  );
};

export default NavBar;
