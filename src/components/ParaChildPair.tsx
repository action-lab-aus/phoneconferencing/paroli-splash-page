import { Heading, Stack, Text } from "@chakra-ui/react";
import { ReactNode } from "react";
import "../App.css";

interface Props {
  title: string;
  para: string;
  invert: boolean;
  children: ReactNode;
}

const ParaChildPair = ({ title, para, children, invert }: Props) => {
  return (
    <Stack
      className="paraPair"
      direction={{ base: invert ? "column" : "column-reverse", lg: "row" }}
    >
      {invert && children}
      <div className={invert ? "useBlurbRight" : "useBlurbLeft"}>
        <Heading as="h4">{title}</Heading>
        <Text>{para}</Text>
      </div>
      {!invert && children}
    </Stack>
  );
};

export default ParaChildPair;
