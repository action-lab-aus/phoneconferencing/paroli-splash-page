import { Heading, VStack, Text, Image } from "@chakra-ui/react";
import "../App.css";

interface Props {
  name: string;
  blurb: string;
  image: string;
}

const PersonProfile = ({ name, blurb, image }: Props) => {
  return (
    <VStack className="profile">
      <Image src={image} objectFit="scale-down" />
      <Heading as="h5">{name}</Heading>
      <Text textAlign="center">{blurb}</Text>
    </VStack>
  );
};

export default PersonProfile;
