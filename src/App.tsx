import {
  Box,
  Container,
  Grid,
  GridItem,
  Stack,
  Heading,
  Image,
  Text,
  SimpleGrid,
  Show,
} from "@chakra-ui/react";
import NavBar from "./components/NavBar";
import ParaChildPair from "./components/ParaChildPair";
import PersonProfile from "./components/PersonProfile";
import "./App.css";

// IMPORT IMAGES
import banner_img from "./assets/banner-image.png";
import internet_img from "./assets/no-internet-icon.png";
import tricks_img from "./assets/newtricks-icon.png";
import voice_img from "./assets/community-voice-icon.png";
import edu_img from "./assets/education-image.png";
import consult_img from "./assets/consultation-image.png";
import townhall_img from "./assets/townhalls-image.png";
import person_dan from "./assets/people/dan.jpg";
import person_adnan from "./assets/people/Md-Adnan-Islam.png";
import person_bron from "./assets/people/bron.jpg";
import person_pranita from "./assets/people/pranita.jpg";
import person_delvin from "./assets/people/delvin.jpg";
import person_tom from "./assets/people/tom.jpg";
import person_patrick from "./assets/people/patrick.png";
import person_manika from "./assets/people/manika.png";
import person_rafi from "./assets/people/rafi.jpg";
import person_emran from "./assets/people/emran.jpg";
import person_shanta from "./assets/people/shanta.jpg";
import person_yousuf from "./assets/people/yousuf.jpg";
import contact_img from "./assets/contactus-image.png";
import code_img from "./assets/code_image.png";
import logo from "./assets/logo_smol.png";
import step1 from "./assets/steps/step1.png";
import step2 from "./assets/steps/step2.png";
import step3 from "./assets/steps/step3.png";
import step4 from "./assets/steps/step4.png";
import step5 from "./assets/steps/step5.png";


function App() {
  return (
    <Grid templateAreas={`"nav" "main" "footer"`}>
      <GridItem area="nav">
        <NavBar />
      </GridItem>
      <GridItem area="main">
        {/* FIRST SECTION */}
        <div className="sectionOdd">
          <Container maxW="160ch">
            <Stack
              direction={{ base: "column", lg: "row" }}
              justify="space-between"
              className="headerSection"
            >
              <Image
                className="heroImg"
                objectFit="scale-down"
                src={banner_img}
              />
              <div className="headerText">
                <Heading
                  size="3xl"
                  as="h2"
                  marginTop="40px"
                  marginBottom="40px"
                >
                  Paroli makes it easy to host remote activities over the phone.
                </Heading>
                <Text fontSize="2xl">
                  Paroli supports running interactive remote engagements with
                  hard-to-reach communities using standard telephone calls.
                </Text>
              </div>
            </Stack>
          </Container>
        </div>

        {/* SECOND SECTION */}
        <div className="sectionEven" id="about">
          
            <Heading as="h3" marginBottom="20px" textAlign={"center"}>
              About Paroli
            </Heading>

            <iframe 
            className="video"
            src="https://www.youtube.com/embed/OamV55SwKNc" 
            title="YouTube video player"
            allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share; fullscreen;" >
            </iframe>

            <Container maxW="95ch">
            <Text fontSize="xl" className="para">
              Developed in response to the COVID-19 pandemic, Paroli uses phone
              calls to support engagements with remote communities who often
              lack access to the Internet or strong literacy skills.
            </Text>
            <Text fontSize="xl" className="para">
              Invited participants receive a phone call from the Paroli
              platform, letting them join the discussion free of charge.
            </Text>
            <Text fontSize="xl" className="para">
              The only person who needs internet access is the call's host, who
              uses the website to manage the call through actions like muting
              and unmuting participants, as well as access recordings and stats
              from previous calls.
            </Text>
            <Text fontSize="xl" className="para">
              Paroli is an ongoing research project at Monash University's
              Action Lab.<br/> The research and development of Paroli was funded
              by a grant to Monash University and Oxfam Bangladesh by the 
              Empowerment Trust, for the project "<i>PROTIC 2: Participatory Research
              and Ownership with Technology, Information and Change</i>". 
              <br/>If you are interested in taking part, please contact
              <a href="mailto:info@actionlab.dev">
                {" "}
                info@actionlab.dev
              </a>.
            </Text>
          </Container>
        </div>

        {/* THIRD SECTION */}
        <div className="sectionOdd" id="features">
          <Container maxW="110ch">
            <Heading as="h3" textAlign={"center"} marginBottom="40px">
              Core Features
            </Heading>
            <Stack
              direction={{ base: "column", md: "row" }}
              justify="space-between"
              spacing="14px"
            >
              <Box className="featureBox">
                <Image src={internet_img} />
                <Heading as="h4">No Internet? No Problem.</Heading>
                <Text>
                  Because Paroli uses standard phone calls, participants don't
                  need to have access to the internet to take part. Only the
                  call's host needs to be able to access the Paroli website,
                  which can be used on a desktop, laptop, tablet or smartphone.
                </Text>
              </Box>

              <Box className="featureBox">
                <Image src={tricks_img} />
                <Heading as="h4">Old Dog, New Tricks</Heading>
                <Text>
                  Because it uses standard phone calls, participants can use
                  Paroli with everything from a rotary phone to the latest
                  iPhone. Using the numbered buttons on their handset,
                  participants can mute themselves, vote in polls, or put their
                  hand up to request to speak. We're even working to develop
                  exciting new features such as breakout rooms, which will allow
                  hosts to run activities previously impossible over the phone.
                </Text>
              </Box>

              <Box className="featureBox">
                <Image src={voice_img} />
                <Heading as="h4">Community Voice</Heading>
                <Text>
                  Paroli is designed to support knowledge sharing between
                  organisations and under-represented communities, and can be
                  used for everything from remote learning to consultation and
                  research. As well as collecting participation statistics,
                  Paroli records each person’s audio separately: helping with
                  both data analysis and the creation of follow-up media, such
                  as podcasts.
                </Text>
              </Box>
            </Stack>
          </Container>
        </div>

        {/* FOURTH SECTION */}
        <div className="sectionEven" id="usecases">
          <Container maxW="110ch">
            <Heading as="h3" marginBottom="20px" textAlign={"center"}>
              What can Paroli be used for?
            </Heading>

            <ParaChildPair
              title="Education and Remote Learning"
              para="Paroli has been used as a remote classroom, where the host
              acts as the teacher. Because the engagement takes place over
              the phone, special guests can be conveniently called to
              contribute their expertise from their normal workplace. The
              hand-raising and mute/unmute functionalities also make it easy
              to run audience Question & Answer segments."
              invert={false}
            >
              <Image src={edu_img} objectFit="scale-down" />
            </ParaChildPair>

            <ParaChildPair
              title="Community Consultation & Research"
              para="Paroli is designed to support engaging with hard-to-reach
              communities in order to understand their needs and opinions as
              stakeholders. After a call has finished, the host is able to
              view detailed logs about participants' activities and
              contributions during the call, and can access separate
              recordings for each participant. Upcoming features, such as
              breakout rooms, will enable researchers to hold group
              activities over the phone: perfect for research workshops and
              focus group discussions."
              invert={true}
            >
              <Image src={consult_img} objectFit="scale-down" />
            </ParaChildPair>

            <ParaChildPair
              title="Town halls & Community Radio"
              para="Paroli's simple interface makes it suitable for use by
              communities wanting to run their own remote activities, such
              as 'radio' shows or town hall meetings. Paroli helps hosts
              prepare and keep time to a meeting agenda, take notes, and its
              recording features make it easier to produce podcasts and
              audio summaries."
              invert={false}
            >
              <Image src={townhall_img} objectFit="scale-down" />
            </ParaChildPair>
          </Container>
        </div>

        {/* FIFTH SECTION */}
        <div className="sectionOdd">
          <Container maxW="110ch">
            <Heading as="h3" textAlign={"center"} marginBottom="40px">
              How do you use Paroli?
            </Heading>

            <ParaChildPair
              title="Step 1: Get Access"
              para="As Paroli is currently invite-only, the host first contacts Action Lab to get approved to use the Paroli platform. Alternatively, you can set up your own installation (see below)."
              invert={false}
            >
              <Image src={step1} objectFit="scale-down" className="stepImg" />
            </ParaChildPair>

            <ParaChildPair
              title="Step 2: Put a Crew Together"
              para="The host creates a contact list of people they want to include in calls. Contacts can even be added into groups, to make it easier to run regular calls with the same set of participants."
              invert={false}
            >
              <Image src={step2} objectFit="scale-down" className="stepImg" />
            </ParaChildPair>

            <ParaChildPair
              title="Step 3: Get Organised"
              para="The host schedules a new call: giving it a title, start time, a list of attendees and, optionally, an agenda. The host also chooses their desired privacy settings, which control who is able to dial in and join the call."
              invert={false}
            >
              <Image src={step3} objectFit="scale-down" className="stepImg" />
            </ParaChildPair>

            <ParaChildPair
              title="Step 4: Go Live"
              para="The host starts the call. They receive a phone call first, and then are able to dial the selected attendees. During the call, the host can add, remove, mute and unmute callers, see participants who have their hand up to ask a question, advance the agenda, or start a poll for participants to vote on."
              invert={false}
            >
              <Image src={step4} objectFit="scale-down" className="stepImg" />
            </ParaChildPair>

            <ParaChildPair
              title="Step 5: Review"
              para="The host ends the call, disconnecting the attendees. They can then review the events of the call, see who spoke the most, and access recordings of each participant."
              invert={false}
            >
              <Image src={step5} objectFit="scale-down" className="stepImg" />
            </ParaChildPair>
          </Container>
        </div>

        {/* SIXTH SECTION */}
        <div className="sectionEven">
          <Heading as="h3" marginBottom="50px" textAlign={"center"}>
            Meet the Team
          </Heading>

          <Heading as="h4" textAlign={"center"} className="teamName">
            Action Lab
          </Heading>

          <Container maxW="110ch" textAlign="center">
          Action Lab is a world-leading Human-Computer Interaction research group based in Monash University, Melbourne. We work closely with communities and organisations around the world to design, develop and find ways to re-appropriate technologies that address their needs as stakeholders.
          </Container>

          <div className="peopleContainer">
          <PersonProfile
              name="Md. Adnanul Islam"
              blurb="Adnan is a PhD candidate at the Department of Human Centred Computing, working on designing collaborative systems for marginalised communities."
              image={person_adnan}
            />
            <PersonProfile
              name="Dr Dan Richardson"
              blurb="Dan is an experienced Human-Computer Interaction researcher and developer, with a focus on building collaborative systems through participatory design."
              image={person_dan}
            />

            <PersonProfile
              name="Dr Manika Saha"
              blurb="Manika's research focuses on human-centric design approaches and engaging governments, INGOs, and marginalized communities in project design and ICT for development."
              image={person_manika}
            />

            <PersonProfile
              name="Dr Bronwyn Cumbo"
              blurb="Bronwyn is an expert in participatory design and research methods, with a focus on enabling sustainability with marginalised groups."
              image={person_bron}
            />

            <PersonProfile
              name="Pranita Shrestha"
              blurb="Pranita is a PhD candidate at the Department of Human Centred Computing, investigating interdisciplinary approaches combining health and technology."
              image={person_pranita}
            />

            <PersonProfile
              name="Dr Delvin Varghese"
              blurb="Delvin's research focuses on equitable digital processes, working in partnership with government organisations and NGOs."
              image={person_delvin}
            />

            <PersonProfile
              name="Dr Tom Bartindale"
              blurb="Tom has worked closely with community and NGO partners to design and deliver novel interaction technologies, with a focus on multimedia and telephony."
              image={person_tom}
            />

            <PersonProfile
              name="Prof Patrick Olivier"
              blurb="A leader within the field of Human-Computer Interaction, Patrick has particular expertise in technology research that crosses boundaries across multiple disciplines."
              image={person_patrick}
            />
          </div>

          <Heading as="h4" marginTop="50px" textAlign={"center"} className="teamName">
            Oxfam in Bangladesh
          </Heading>

          <Container maxW="110ch" textAlign="center">
            Oxfam in Bangladesh aims to create a vibrant and equitable society across Bangladesh, where women and men are empowered, jointly exercising leadership and good governance towards building resilient communities. They work with a wide range of partners that include civil society organizations, NGOs, media organizations, foreign and local universities, private sector companies, along with different levels of government.
          </Container>

          <div className="peopleContainer">
            <PersonProfile
              name="Md. Emran Hasan"
              blurb="Emran specializes in ecosystems, climate change, Natural Resource Management, conservation, disaster preparedness, resilience and human-environment safeguard management."
              image={person_emran}
            />
            <PersonProfile
              name="Rafiul Alam"
              blurb="Rafiul is development professional and youth researcher focusing on participatory GIS, human-environmental nexus, and community-centred interventions."
              image={person_rafi}
            />

            <PersonProfile
              name="Shanta Soheli Moyna"
              blurb="Shanta promotes inclusive transboundary water governance, supporting marginalized women and youth leadership dependent on natural resources to claim their rights."
              image={person_shanta}
            />
            <PersonProfile
              name="Mohammad Yousuf"
              blurb="Mohammad is committed to ending gender inequality, and ensuring fair wages for women informal workers through digital inclusion."
              image={person_yousuf}
            />
          </div>


        </div>

        {/* SEVENTH SECTION */}
        <div className="contactContainer" id="contact">
          <Show above="lg">
            <Image src={contact_img} className="contactImg" />
          </Show>

          <Box className="contactBox">
            <Image src={logo} width="110px" className="contactImg" />
            <Heading as="h4">Contact Us</Heading>
            <Text>
              To find out more about Paroli, get support for your own
              deployment, or get involved in our ongoing research, please
              contact Md. Adnanul Islam at
              <a href="mailto:info@actionlab.dev">
                {" "}
                info@actionlab.dev
              </a>
            </Text>
          </Box>
        </div>

        {/* EIGHTH SECTION */}
        <div className="sectionEven" id="code">
          <Image src={code_img} className="codeImg" />

          <div className="codeSection">
            <Container>
              <Heading as="h4" marginBottom="20px">
                Set up your own installation
              </Heading>
              <Text className="para">
                Paroli is an open source project, published under the MIT
                license. That means you can freely download, copy, edit and even
                profit from the Paroli codebase.
                <br />A guide to how to get started can be found at{" "}
                <a href="https://docs.paroli.live" target="_blank">
                  docs.paroli.live
                </a>
                .<br />
                You can access all of the relevant code on our Git repository{" "}
                <a
                  href="https://gitlab.com/action-lab-aus/phoneconferencing"
                  target="_blank"
                >
                  here
                </a>
                .
              </Text>
            </Container>
          </div>
        </div>
      </GridItem>

      <GridItem area="footer" className="footer">
        <Container>
          <Text>
            <b>Developed by</b> <br /> Action Lab, Monash University
          </Text>
        </Container>
      </GridItem>
    </Grid>
  );
}

export default App;
